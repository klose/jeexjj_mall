<#--
/****************************************************
 * Description: 商品类目的简单列表页面，没有编辑功能
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-list.ftl"> 
<@list id=tabId>
	<thead>
		<tr>
			<th><input type="checkbox" class="bscheckall"></th>
	        <th>父分类ID=0时代表一级根分类</th>
	        <th>分类名称</th>
	        <th>状态 1启用 0禁用</th>
	        <th>排列序号</th>
	        <th>是否为父分类 1为true 0为false</th>
	        <th>图标</th>
	        <th>备注</th>
	        <th>创建时间</th>
	        <th>更新时间</th>
	        <th>操作</th>
		</tr>
	</thead>
	<tbody>
		<#list page.items?if_exists as item>
		<tr>
			<td>
			<input type="checkbox" class="bscheck" data="id:${item.id}">
			</td>
			<td>
			    ${item.parentId}
			</td>
			<td>
			    ${item.name}
			</td>
			<td>
			    ${item.status}
			</td>
			<td>
			    ${item.sortOrder}
			</td>
			<td>
			    ${item.isParent}
			</td>
			<td>
			    ${item.icon}
			</td>
			<td>
			    ${item.remark}
			</td>
			<td>
			    ${item.created?string('yyyy-MM-dd HH:mm:ss')}
			</td>
			<td>
			    ${item.updated?string('yyyy-MM-dd HH:mm:ss')}
			</td>
			<td>
            	<@button type="purple" icon="fa fa-pencil" onclick="XJJ.edit('${base}/mall/item/cat/input/${item.id}','修改商品类目','${tabId}');">修改</@button>
				<@button type="danger" icon=" fa fa-trash-o" onclick="XJJ.del('${base}/mall/item/cat/delete/${item.id}','删除商品类目？',false,{id:'${tabId}'});">删除</@button>
            </td>
		</tr>
		</#list>
	</tbody>
</@list>